using System.IO;
using System.Threading.Tasks;
using BooksApi.DataAccess.Entities;
using BooksApi.Web.ViewModels;

namespace BooksApi.Web.Utility.Mapping
{
    public static class BookMapping
    {
        public static async Task<BookEntity> UploadModelToEntity(BookUploadModel bookUploadModel)
        {
            var bookEntity = new BookEntity()
            {
                Price = bookUploadModel.Price,
                Description = bookUploadModel.Description,
                CreatedAt = bookUploadModel.CreatedAt,
                DeadLine = bookUploadModel.DeadLine,
                Id = bookUploadModel.Id,
                Name = bookUploadModel.Name,
                Status = bookUploadModel.Status,
                UpdatedAt = bookUploadModel.UpdatedAt
            };
            if (bookUploadModel.Image != null)
            {
                await using var memoryStream = new MemoryStream();
                await bookUploadModel.Image.CopyToAsync(memoryStream);
                bookEntity.Image = memoryStream.ToArray();
            }

            return bookEntity;
        }
    }
}