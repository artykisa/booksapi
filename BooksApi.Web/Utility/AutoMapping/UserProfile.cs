using AutoMapper;
using BooksApi.DataAccess.Entities;
using BooksApi.Services.Models;
using BooksApi.Web.ViewModels;

namespace BooksApi.Web.Utility.AutoMapping
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserEntity, UserViewModel>()
                .ForMember(member => member.Id, expression => expression.MapFrom(entity => entity.Id))
                .ForMember(member => member.Email, expression => expression.MapFrom(entity => entity.Email))
                .ForMember(member => member.Bonuses, expression => expression.MapFrom(entity => entity.Bonuses))
                .ForMember(member => member.Role, expression => expression.MapFrom(entity => entity.Role));
            CreateMap<UserLoginModel, UserEntity>();
        }
    }
}