using System.Collections.Generic;
using AutoMapper;
using BooksApi.DataAccess.Entities;
using BooksApi.Services.Models;
using BooksApi.Web.ViewModels;

namespace BooksApi.Web.Utility.AutoMapping
{
    public class CartProfile : Profile
    {
        public CartProfile()
        {
            CreateMap<IList<BookEntity>, CartModel>()
                .ForMember(c => c.UserId, exp => exp.Ignore())
                .ForMember(c => c.Books, exp => exp.MapFrom(b => b));
            CreateMap<CartModel, CartViewModel>();
        }
    }
}