using AutoMapper;
using BooksApi.DataAccess.Entities;
using BooksApi.Services.Models;

namespace BooksApi.Web.Utility.AutoMapping
{
    public class OrderProfile: Profile
    {
        public OrderProfile()
        {
            CreateMap<OrderEntity, OrderModel>();
        }
    }
}