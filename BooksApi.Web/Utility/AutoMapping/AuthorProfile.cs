using AutoMapper;
using BooksApi.DataAccess.Entities;
using BooksApi.Web.ViewModels;

namespace BooksApi.Web.Utility.AutoMapping
{
    public class AuthorProfile: Profile
    {
        public AuthorProfile()
        {
            CreateMap<AuthorEntity, AuthorViewModel>();
        }
    }
}