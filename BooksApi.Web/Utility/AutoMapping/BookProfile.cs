using System.IO;
using AutoMapper;
using BooksApi.DataAccess.Entities;
using BooksApi.Services.Models;
using BooksApi.Web.ViewModels;
using Microsoft.AspNetCore.Http;

namespace BooksApi.Web.Utility.AutoMapping
{
    public class BookProfile : Profile
    {
        public BookProfile()
        {
            CreateMap<BookEntity, BookViewModel>();
            CreateMap<BookViewModel, BookEntity>();
            CreateMap<CartBookModel, CartBookViewModel>();
            CreateMap<BookEntity, CartBookModel>()
                .ForMember(cart => cart.Count, exp => exp.Ignore());
        }
    }
}