using System.Collections.Generic;
using BooksApi.Services.Models;

namespace BooksApi.Web.ViewModels
{
    public class CartViewModel
    {
        public int UserId { get; set; }
        public IList<CartBookViewModel> Books { get; set; }
    }
}