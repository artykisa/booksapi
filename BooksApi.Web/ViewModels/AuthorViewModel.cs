using System;
using System.Collections.Generic;

namespace BooksApi.Web.ViewModels
{
    public class AuthorViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime BornDate  {get; set; }
        public DateTime DiedDate { get; set; }
        public List<BookViewModel> Books { get; set; }
    }
}