using System;
using BooksApi.DataAccess.Enums;

namespace BooksApi.Web.ViewModels
{
    public class CartBookViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt  {get; set; }
        public DateTime UpdatedAt { get; set; }
        public BookStatus Status { get; set; }
        public DateTime DeadLine { get; set; }
        public byte[] Image { get; set; }
        public AuthorViewModel Author { get; set; }
        public int? Count { get; set; }
    }
}