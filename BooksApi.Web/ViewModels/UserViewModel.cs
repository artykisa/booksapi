using BooksApi.DataAccess.Enums;

namespace BooksApi.Web.ViewModels
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public double Bonuses { get; set; }
        public UserRole Role { get; set; }
    }
}