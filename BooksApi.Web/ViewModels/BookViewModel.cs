using System;
using BooksApi.DataAccess.Entities;
using BooksApi.DataAccess.Enums;
using Microsoft.AspNetCore.Http;

namespace BooksApi.Web.ViewModels
{
    public class BookViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt  {get; set; }
        public DateTime UpdatedAt { get; set; }
        public BookStatus Status { get; set; }
        public DateTime DeadLine { get; set; }
        public byte[] Image { get; set; }
        public AuthorViewModel Author { get; set; }
        public int? Count { get; set; }
    }
}