using BooksApi.DataAccess.Enums;

namespace BooksApi.Web.ViewModels
{
    public class OrderUpdateModel
    {
        public int Id { get; set; }
        public OrderStatus Status { get; set; }
    }
}