using System.Collections.Generic;
using AutoMapper;
using BooksApi.DataAccess.Entities;
using BooksApi.DataAccess.Enums;
using BooksApi.Services.Services.Interfaces;
using BooksApi.Web.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BooksApi.Web.Controllers
{
    [Route("api/authors")]
    [Produces("application/json")]
    [Authorize]
    public class AuthorController : Controller
    {
        private readonly IAuthorService _authorService;
        private readonly IMapper _mapper;

        public AuthorController(IAuthorService authorService, IMapper mapper)
        {
            _authorService = authorService;
            _mapper = mapper;
        }
        
        /// <summary>
        /// Get all authors.
        /// </summary>
        [HttpGet]
        public IActionResult GetAuthors()
        {
            var authors = _authorService.GetAuthors();
            var authorsViewModel = _mapper.Map<List<AuthorViewModel>>(authors);
            return Ok(authorsViewModel);
        }

        /// <summary>
        /// Add author to list.
        /// </summary>
        [HttpPost]
        [Authorize(Roles = UserRoleExtensions.AdminRole)]
        public IActionResult RegisterAuthor(AuthorEntity authorEntity)
        {
            var returnAuthorEntity = _authorService.RegisterAuthor(authorEntity);
            if (returnAuthorEntity == null)
            {
                return BadRequest();
            }
            return Ok(returnAuthorEntity);
        }
        
        /// <summary>
        /// Update author.
        /// </summary>
        [HttpPut]
        [Authorize(Roles = UserRoleExtensions.AdminRole)]
        public IActionResult UpdateAuthor(AuthorEntity authorEntity)
        {
            var updateAuthorEntity = _authorService.GetAuthor(authorEntity.Id);
            if (updateAuthorEntity == null)
            {
                return BadRequest();
            }

            updateAuthorEntity.Description = authorEntity.Description;
            updateAuthorEntity.Name = authorEntity.Name;
            updateAuthorEntity.BornDate = authorEntity.BornDate;
            updateAuthorEntity.DiedDate = authorEntity.DiedDate;

            var returnAuthorEntity = _authorService.UpdateAuthor(updateAuthorEntity);
            
            return Ok(returnAuthorEntity);
        }

        /// <summary>
        /// Get author by id.
        /// </summary>
        /// <param name="id">Author's id.</param>
        [HttpGet]
        [Route("{id:int}")]
        public IActionResult GetAuthor(int id)
        {
            var author = _authorService.GetAuthor(id);
            var authorViewModel = _mapper.Map<AuthorViewModel>(author);
            if (authorViewModel == null)
            {
                return NotFound();
            }
            return Ok(authorViewModel);
        }
        
        /// <summary>
        /// Delete author by id.
        /// </summary>
        /// <param name="id">Author's id.</param>
        [HttpDelete]
        [Route("{id:int}")]
        public IActionResult DeleteAuthor(int id)
        {
            var author = _authorService.GetAuthor(id);
            if (author == null)
            {
                return NotFound();
            }
            _authorService.DeleteAuthor(id);
            return Ok();
        }
    }
}