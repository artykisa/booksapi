using System.Collections.Generic;
using AutoMapper;
using BooksApi.DataAccess.Entities;
using BooksApi.DataAccess.Enums;
using BooksApi.DataAccess.Repositories.Interfaces;
using BooksApi.Services.Services.Interfaces;
using BooksApi.Web.Utility.Mapping;
using BooksApi.Web.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BooksApi.Web.Controllers
{
    [Route("api/books")]
    [Produces("application/json")]
    [Authorize]
    public class BooksController : Controller
    {
        private readonly IBookService _bookService;
        private readonly IMapper _mapper;
        private readonly IAuthorService _authorService;
        private readonly ICartRepository _cartRepository;
        private readonly IUserService _userService;

        public BooksController(IBookService bookService, IMapper mapper, IAuthorService authorService, ICartRepository cartRepository, IUserService userService)
        {
            _bookService = bookService;
            _mapper = mapper;
            _authorService = authorService;
            _cartRepository = cartRepository;
            _userService = userService;
        }

        /// <summary>
        /// Get all books.
        /// </summary>
        [HttpGet]
        public IActionResult GetBooks()
        {
            var books = _bookService.GetBooks();
            var booksViewModel = _mapper.Map<List<BookViewModel>>(books);
            var userId = _userService.GetUserIdFromJWT(User);
            if (userId.HasValue)
            {
                foreach (var book in booksViewModel)
                {
                    book.Count = _cartRepository.Get(userId.Value, book.Id);
                }
            }
            return Ok(booksViewModel);
        }

        /// <summary>
        /// Add book to list.
        /// </summary>
        [HttpPost]
        [Authorize(Roles = UserRoleExtensions.AdminRole)]
        public IActionResult RegisterBook(BookUploadModel bookUploadModel)
        {
            var bookEntity = BookMapping.UploadModelToEntity(bookUploadModel);
            bookEntity.Result.Author = _authorService.GetAuthor(bookUploadModel.AuthorId);
            
            var returnBookEntity = _bookService.RegisterBook(bookEntity.Result);
            if (returnBookEntity == null)
            {
                return BadRequest();
            }
            var returnBookViewModel = _mapper.Map<BookViewModel>(returnBookEntity);
            return Ok(returnBookViewModel);
        }

        /// <summary>
        /// Get book by id.
        /// </summary>
        /// <param name="id">Book's id.</param>
        [HttpGet]
        [Route("{id:int}")]
        public IActionResult GetBook(int id)
        {
            var book = _bookService.GetBook(id);
            if (book == null)
            {
                return NotFound();
            }
            var bookViewModel = _mapper.Map<BookViewModel>(book);
            var userId = _userService.GetUserIdFromJWT(User);
            if (userId.HasValue)
            {
                bookViewModel.Count = _cartRepository.Get(userId.Value, book.Id);
            }
            return Ok(bookViewModel);
        }
        
        /// <summary>
        /// Delete book by id.
        /// </summary>
        /// <param name="id">Book's id.</param>
        [HttpDelete]
        [Route("{id:int}")]
        public IActionResult DeleteBook(int id)
        {
            var book = _bookService.GetBook(id);
            if (book == null)
            {
                return NotFound();
            }
            _bookService.DeleteBook(id);
            return Ok();
        }

        /// <summary>
        /// Get books where deadline hasn't arrived.
        /// </summary>
        [HttpGet]
        [Route("available")]
        public IActionResult GetAvailableBooks()
        {
            var books = _bookService.GetAvailableBooks();
            var booksViewModel = _mapper.Map<List<BookViewModel>>(books);
            return Ok(booksViewModel);
        }
        
        /// <summary>
        /// Get books where deadline has arrived.
        /// </summary>
        [HttpGet]
        [Route("expired")]
        public IActionResult GetExpiredBooks()
        {
            var books = _bookService.GetExpiredBooks();
            var booksViewModel = _mapper.Map<List<BookViewModel>>(books);
            return Ok(booksViewModel);
        }
        
        /// <summary>
        /// Update book.
        /// </summary>
        [HttpPut]
        public IActionResult UpdateBook(BookUploadModel bookUploadModel)
        {
            var bookEntity = BookMapping.UploadModelToEntity(bookUploadModel);
            bookEntity.Result.Author = _authorService.GetAuthor(bookUploadModel.AuthorId);
            
            var book = _bookService.GetBook(bookUploadModel.Id);
            book.Description = bookEntity.Result.Description;
            book.Image = bookEntity.Result.Image;
            book.Name = bookEntity.Result.Name;
            book.Status = bookEntity.Result.Status;
            book.CreatedAt = bookEntity.Result.CreatedAt;
            book.UpdatedAt = bookEntity.Result.UpdatedAt;
            book.DeadLine = bookEntity.Result.DeadLine;
            book.Price = bookEntity.Result.Price;

            var returnBookEntity = _bookService.UpdateBook(book);
            if (returnBookEntity == null)
            {
                return BadRequest();
            }
            var returnBookViewModel = _mapper.Map<BookViewModel>(returnBookEntity);
            return Ok(returnBookViewModel);
        }
        
    }
}