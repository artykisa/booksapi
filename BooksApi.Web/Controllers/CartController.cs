using AutoMapper;
using BooksApi.Services.Models;
using BooksApi.Services.Services;
using BooksApi.Services.Services.Interfaces;
using BooksApi.Web.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BooksApi.Web.Controllers
{
    [Route("api/cart")]
    [Produces("application/json")]
    [Authorize]
    public class CartController : Controller
    {
        private readonly ICartService _cartService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public CartController(ICartService cartService, IUserService userService, IMapper mapper)
        {
            _cartService = cartService;
            _userService = userService;
            _mapper = mapper;
        }

        /// <summary>
        /// Get current user's cart.
        /// </summary>
        [HttpGet]
        public IActionResult GetCart()
        {
            var userId = _userService.GetUserIdFromJWT(User);
            if (userId == null)
            {
                return BadRequest();
            }

            var cartEntity = _cartService.GetCart(userId.Value);
            var cartViewModel = _mapper.Map<CartViewModel>(cartEntity);
            cartViewModel.UserId = userId.Value;
            return Ok(cartViewModel);
        }

        /// <summary>
        /// Clean current user's cart.
        /// </summary>
        [HttpPost]
        [Route("clean")]
        public IActionResult CleanCart()
        {
            var userId = _userService.GetUserIdFromJWT(User);
            if (userId == null)
            {
                return BadRequest();
            }

            var cartEntity = _cartService.CleanCart(userId.Value);
            var cartViewModel = _mapper.Map<CartViewModel>(cartEntity);
            return Ok(cartViewModel);
        }

        /// <summary>
        /// Add book to current user's cart.
        /// </summary>
        /// <param name="bookId">Book's id.</param>
        [HttpPost]
        [Route("{bookId:int}")]
        public IActionResult AddBook(int bookId)
        {
            var userId = _userService.GetUserIdFromJWT(User);
            if (userId == null)
            {
                return BadRequest();
            }

            var cartEntity = _cartService.AddBook(userId.Value, bookId);
            var cartViewModel = _mapper.Map<CartViewModel>(cartEntity);
            if (cartViewModel == null)
            {
                return BadRequest();
            }
            return Ok(cartViewModel);
        }
        
        /// <summary>
        /// Delete one copy of the book from current user's cart.
        /// </summary>
        /// <param name="bookId">Book's id.</param>
        [HttpDelete]
        [Route("{bookId:int}")]
        public IActionResult RemoveOneBook(int bookId)
        {
            var userId = _userService.GetUserIdFromJWT(User);
            if (userId == null)
            {
                return BadRequest();
            }

            var cartEntity = _cartService.RemoveOneBook(userId.Value, bookId);
            var cartViewModel = _mapper.Map<CartViewModel>(cartEntity);
            return Ok(cartViewModel);
        }
        
        /// <summary>
        /// Delete all copies of the book from current user's cart.
        /// </summary>
        /// <param name="bookId">Book's id.</param>
        [HttpDelete]
        [Route("remove/{bookId:int}")]
        public IActionResult RemoveBook(int bookId)
        {
            var userId = _userService.GetUserIdFromJWT(User);
            if (userId == null)
            {
                return BadRequest();
            }

            var cartEntity = _cartService.RemoveBook(userId.Value, bookId);
            var cartViewModel = _mapper.Map<CartViewModel>(cartEntity);
            return Ok(cartViewModel);
        }
        
        /// <summary>
        /// Order cart. Cost will be 'Cart count' - bonuses.
        /// After buy user will get 5% of cost as bonuses.
        /// </summary>
        /// <param name="bonuses">Number of bonuses to use.</param>
        /// <param name="address">Address to ship.</param>
        [HttpPost]
        [Route("order")]
        public IActionResult OrderCart(CartOrderModel cartOrderModel)
        {
            var userId = _userService.GetUserIdFromJWT(User);
            if (userId == null)
            {
                return BadRequest();
            }

            var cartEntity = _cartService.OrderCart(userId.Value, cartOrderModel);
            if(cartEntity == null)
            {
                return BadRequest();
            }

            var cartViewModel = _mapper.Map<CartViewModel>(cartEntity);
            return Ok(cartViewModel);
        }
    }
}