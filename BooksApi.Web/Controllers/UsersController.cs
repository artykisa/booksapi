using System.Collections.Generic;
using AutoMapper;
using BooksApi.DataAccess.Entities;
using BooksApi.DataAccess.Enums;
using BooksApi.Services.Models;
using BooksApi.Services.Services.Interfaces;
using BooksApi.Web.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BooksApi.Web.Controllers
{
    [Route("api/users")]
    [Produces("application/json")]
    public class UsersController : Controller
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UsersController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }
        
        /// <summary>
        /// Get all users.
        /// </summary>
        [HttpGet]
        public IActionResult GetUsers()
        {
            var users = _userService.GetUsers();
            var userViewModels = _mapper.Map<IList<UserViewModel>>(users);
            return Ok(userViewModels);
        }

        /// <summary>
        /// Get your User Model.
        /// </summary>
        [HttpGet]
        [Route("user")]
        [Authorize]
        public IActionResult GetUser()
        {
            var userId = _userService.GetUserIdFromJWT(User);
            if (userId == null)
            {
                return BadRequest();
            }

            return GetUser(userId.Value);
        }
        
        /// <summary>
        /// Get user via id.
        /// </summary>
        /// <param name="id">User's id.</param>
        [HttpGet]
        [Route("{id:int}")]
        public IActionResult GetUser(int id)
        {
            var user = _userService.GetUser(id);
            if (user == null)
            {
                return NotFound();
            }

            var userViewModel = _mapper.Map<UserViewModel>(user);
            return Ok(userViewModel);
        }
        
        /// <summary>
        /// Give user admin's permissions.
        /// </summary>
        /// <param name="id">User's id.</param>
        [HttpPut]
        [Route("admin/{id:int}")]
        [Authorize(Roles = UserRoleExtensions.AdminRole)]
        public IActionResult GiveUserAdminPermissions(int id)
        {
            var user = _userService.GiveAdminPermissions(id);

            var userViewModel = _mapper.Map<UserViewModel>(user);
            return Ok(userViewModel);
        }
        
        /// <summary>
        /// Remove user's admin permissions.
        /// </summary>
        /// <param name="id">User's id.</param>
        [HttpDelete]
        [Route("admin/{id:int}")]
        [Authorize(Roles = UserRoleExtensions.AdminRole)]
        public IActionResult RemoveUserAdminPermissions(int id)
        {
            var user = _userService.RemoveAdminPermissions(id);

            var userViewModel = _mapper.Map<UserViewModel>(user);
            return Ok(userViewModel);
        }
        
        /// <summary>
        /// Login via email and password. Returns JWT token that could be used to auth-requests "Bearer 'token'".
        /// </summary>
        /// <param name="email">User's email.</param>
        /// <param name="password">User's password.</param>
        [HttpPost]
        [Route("login")]
        public IActionResult Login(UserLoginModel userLoginModel)
        {
            var userEntity = _mapper.Map<UserEntity>(userLoginModel);
            var userAccessModel = _userService.LoginUser(userEntity);
            if (userAccessModel == null)
            {
                return BadRequest();
            }
            return Ok(userAccessModel);
        }
        
        /// <summary>
        /// Register user email and password.
        /// </summary>
        /// <param name="email">User's email.</param>
        /// <param name="password">User's password.</param>
        [HttpPost]
        public IActionResult RegisterUser(UserLoginModel userLoginModel)
        {
            if (!_userService.ValidateUser(userLoginModel))
            {
                return UnprocessableEntity();
            }
            var userEntity = _mapper.Map<UserEntity>(userLoginModel);
            var user = _userService.RegisterUser(userEntity);
            if (user == null)
            {
                return BadRequest();
            }
            return Ok(user);
        }
    }
}