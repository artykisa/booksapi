using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BooksApi.DataAccess.Entities;
using BooksApi.DataAccess.Enums;
using BooksApi.Services.Models;
using BooksApi.Services.Services.Interfaces;
using BooksApi.Web.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BooksApi.Web.Controllers
{
    [Route("api/orders")]
    [Produces("application/json")]
    [Authorize(Roles = UserRoleExtensions.AdminRole)]
    public class OrderController : Controller
    {
        private readonly IOrderService _orderService;
        private readonly IMapper _mapper;

        public OrderController(IOrderService orderService, IMapper mapper)
        {
            _orderService = orderService;
            _mapper = mapper;
        }
        
        /// <summary>
        /// Get all orders.
        /// </summary>
        [HttpGet]
        public IActionResult GetOrders()
        {
            var orders = _orderService.GetAllOrders();
            var orderViewModels = orders.Select(order => new OrderViewModel()
                {
                    Id = order.Id,
                    Status = order.Status,
                    Bonuses = order.Bonuses,
                    Cost = order.Cost,
                    FinalCost = order.FinalCost,
                    Phone = order.Phone,
                    Name = order.Name,
                    Books = _mapper.Map<List<CartBookModel>>(order.Books),
                    User = _mapper.Map<UserViewModel>(order.User),
                    Address = order.Address
                }
            ).ToList();
            return Ok(orderViewModels);
        }
        
        /// <summary>
        /// Get order.
        /// </summary>
        /// <param name="id">Order's id.</param>
        [HttpGet]
        [Route("{id:int}")]
        public IActionResult GetOrder(int id)
        {
            var order = _orderService.GetOrder(id);
            var orderViewModel = new OrderViewModel()
            {
                Id = order.Id,
                Status = order.Status,
                Bonuses = order.Bonuses,
                Cost = order.Cost,
                FinalCost = order.FinalCost,
                Phone = order.Phone,
                Name = order.Name,
                Books = _mapper.Map<List<CartBookModel>>(order.Books),
                User = _mapper.Map<UserViewModel>(order.User),
                Address = order.Address
            };
            return Ok(orderViewModel);
        }
        
        /// <summary>
        /// Update order.
        /// </summary>
        [HttpPut]
        public IActionResult UpdateOrder(OrderUpdateModel orderUpdateModel)
        {
            var order = _orderService.UpdateOrderEntity(new OrderEntity()
            {
                Id = orderUpdateModel.Id,
                Status = orderUpdateModel.Status
            });
            
            var orderViewModel = new OrderViewModel()
            {
                Id = order.Id,
                Status = order.Status,
                Bonuses = order.Bonuses,
                Cost = order.Cost,
                FinalCost = order.FinalCost,
                Phone = order.Phone,
                Name = order.Name,
                Books = _mapper.Map<List<CartBookModel>>(order.Books),
                User = _mapper.Map<UserViewModel>(order.User),
                Address = order.Address
            };
            
            return Ok(orderViewModel);
        }
    }
}