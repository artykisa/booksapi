using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BooksApi.DataAccess.Entities;
using BooksApi.DataAccess.Enums;
using BooksApi.DataAccess.Repositories.Interfaces;
using BooksApi.Services.Models;
using BooksApi.Services.Services.Interfaces;

namespace BooksApi.Services.Services
{
    public class CartService : ICartService
    {
        private readonly IBookRepository _bookRepository;
        private readonly IMapper _mapper;
        private readonly IBookService _bookService;
        private readonly IUserService _userService;
        private readonly ICartRepository _cartRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IMailService _mailService;

        public CartService(IBookRepository bookRepository, IMapper mapper, 
            IBookService bookService, IUserService userService, ICartRepository cartRepository, 
            IOrderRepository orderRepository, IMailService mailService)
        {
            _bookRepository = bookRepository;
            _mapper = mapper;
            _bookService = bookService;
            _userService = userService;
            _cartRepository = cartRepository;
            _orderRepository = orderRepository;
            _mailService = mailService;
        }

        public CartModel GetCart(int userId)
        {
            var books = _bookService.GetBooksFromCart(userId).Select(book => new CartBookModel()
            {
                Id = book.Id,
                CreatedAt = book.CreatedAt,
                DeadLine = book.DeadLine,
                Author = book.Author,
                Image = book.Image,
                Name = book.Name,
                Price = book.Price,
                Description = book.Description,
                Status = book.Status,
                UpdatedAt = book.UpdatedAt,
                Count = _cartRepository.Get(userId, book.Id)
            }).ToList();
            var cartModel = new CartModel()
            {
                UserId = userId,
                Books = books
            };
            return cartModel;
        }

        public CartModel CleanCart(int userId)
        {
            var books = _bookService.GetBooksFromCart(userId);
            foreach (var book in books)
            {
                book.UserCarts = book.UserCarts.Where(user => user.Id != userId).ToList();
                _bookService.UpdateBook(book);
            }
            var cartEntity = GetCart(userId);
            return cartEntity;
        }

        public CartModel AddBook(int userId, int bookId)
        {
            var bookEntity = _bookRepository.GetBook(bookId);
            var userEntity = _userService.GetUser(userId);
            if (bookEntity == null || userEntity == null)
            {
                return null;
            }
            var count = _cartRepository.Get(userId, bookId);
            if (count != null)
            {
                _cartRepository.Update(userId,bookId,count.Value + 1);
            }
            else
            {
                bookEntity.UserCarts.Add(userEntity);
                _bookService.UpdateBook(bookEntity);
                _cartRepository.Update(userId,bookId,1);
            }
            var cartEntity = GetCart(userId);
            return cartEntity;
        }
        
        public CartModel RemoveOneBook(int userId, int bookId)
        {
            var bookEntity = _bookRepository.GetBook(bookId);
            var userEntity = _userService.GetUser(userId);
            var userToDelete = bookEntity.UserCarts.FirstOrDefault(entity => entity.Id == userEntity.Id);
            var count = _cartRepository.Get(userId, bookId);
            if (count > 1)
            {
                _cartRepository.Update(userId,bookId,count.Value - 1);
            }
            else
            {
                bookEntity.UserCarts.Remove(userToDelete);
                _bookService.UpdateBook(bookEntity);
            }
            var cartEntity = GetCart(userId);
            return cartEntity;
        }

        public CartModel RemoveBook(int userId, int bookId)
        {
            var bookEntity = _bookRepository.GetBook(bookId);
            var userEntity = _userService.GetUser(userId);
            var userToDelete = bookEntity.UserCarts.FirstOrDefault(entity => entity.Id == userEntity.Id);
            bookEntity.UserCarts.Remove(userToDelete);
            _bookService.UpdateBook(bookEntity);
            var cartEntity = GetCart(userId);
            
            return cartEntity;
        }

        public CartModel OrderCart(int userId, CartOrderModel cartOrderModel)
        {
            var cart = GetCart(userId);
            var cost = cart.Books.Sum(book => book.Price * book.Count);
            if (cost == null)
            {
                return null;
            }
            
            var userEntity = _userService.GetUser(userId);
            if (cartOrderModel.Bonuses < 0 || cartOrderModel.Bonuses > userEntity.Bonuses)
            {
                return null;
            }

            if (cost * 0.3 < cartOrderModel.Bonuses)
            {
                return null;
            }
            
            var finalCost = cost - cartOrderModel.Bonuses;

            if (cartOrderModel.Bonuses == 0)
            {
                userEntity.Bonuses += finalCost * 0.05;
            }
            else
            {
                userEntity.Bonuses -= cartOrderModel.Bonuses;
            }
            _userService.UpdateUser(userEntity);

            var orderEntity = new OrderEntity()
            {
                Bonuses = cartOrderModel.Bonuses,
                Cost = cost.Value,
                FinalCost = finalCost.Value,
                Name = cartOrderModel.Name,
                Phone = cartOrderModel.Phone,
                User = userEntity,
                Status = OrderStatus.Ordered,
                Books = _bookRepository.GetBooks().Where(book => cart.Books.Any(bookIn => bookIn.Id == book.Id))
                    .ToList(),
                Address = cartOrderModel.Address
            };
            
            var order = _orderRepository.UpdateOrderEntity(orderEntity);
            foreach (var book in orderEntity.Books)
            {
                var count = _cartRepository.Get(userId, book.Id);
                _orderRepository.UpdateCount(order.Id,book.Id,count.Value);
            }
            
            CleanCart(userId);
            
            return GetCart(userId);
        }
    }
}