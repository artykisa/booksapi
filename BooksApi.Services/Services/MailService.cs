using System.Text;
using System.Threading.Tasks;
using BooksApi.DataAccess.Entities;
using BooksApi.Services.Services.Interfaces;
using MailKit.Net.Smtp;
using MimeKit;

namespace BooksApi.Services.Services
{
    public class MailService: IMailService
    {
        private static string FROM_ADDRESS = "infoBooBook@noreply.com";
        private static string FROM_NAME = "BooBook Support";
        
        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();
 
            emailMessage.From.Add(new MailboxAddress(FROM_NAME, FROM_ADDRESS));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };
             
            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.gmail.com", 465, true);
                await client.AuthenticateAsync("123", "123");
                await client.SendAsync(emailMessage);
 
                await client.DisconnectAsync(true);
            }
        }

        public void SendOrderEmail(OrderEntity orderEntity)
        {
            var message = new StringBuilder();
            message.AppendLine($"Cost: {orderEntity.Cost}");
            message.AppendLine($"Final Cost: {orderEntity.FinalCost}");
            message.AppendLine($"Bonuses: {orderEntity.Bonuses}");
            message.AppendLine("Your order: ");
            foreach (var book in orderEntity.Books)
            {
                message.Append($"{book.Name} ");
            }
            
            SendEmailAsync(orderEntity.User.Email, "Order BooBook", message.ToString());
        }
    }
}