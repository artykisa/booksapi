using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using BooksApi.DataAccess.Entities;
using BooksApi.DataAccess.Enums;
using BooksApi.DataAccess.Repositories.Interfaces;
using BooksApi.Services.Helpers;
using BooksApi.Services.Models;
using BooksApi.Services.Services.Interfaces;
using BooksApi.Web.ViewModels;
using Microsoft.IdentityModel.Tokens;

namespace BooksApi.Services.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public UserEntity RegisterUser(UserEntity userEntity)
        {
            userEntity.Bonuses = 0;
            var returnUserEntity = _userRepository.RegisterUser(userEntity);
            return returnUserEntity;
        }

        public UserEntity GetUser(int id)
        {
            return _userRepository.GetUser(id);
        }

        public List<UserEntity> GetUsers()
        {
            return _userRepository.GetUsers();
        }

        public UserAccessModel LoginUser(UserEntity userEntity)
        {
            var user = _userRepository.LoginUser(userEntity);
            if (user == null)
            {
                return null;
            }
            var identity = GetIdentity(user);
            var token = GetJWT(identity);
            return new UserAccessModel()
            {
                AccessToken = token,
                Email = user.Email,
                Id = user.Id
            };
        }

        public UserEntity UpdateUser(UserEntity userEntity)
        {
            var returnBookEntity = _userRepository.UpdateUser(userEntity);
            return returnBookEntity;
        }

        public UserEntity GiveAdminPermissions(int userId)
        {
            var user = GetUser(userId);
            user.Role = UserRole.Admin;
            return UpdateUser(user);
        }

        public UserEntity RemoveAdminPermissions(int userId)
        {
            var user = GetUser(userId);
            user.Role = UserRole.Default;
            return UpdateUser(user);
        }

        public int? GetUserIdFromJWT(ClaimsPrincipal user)
        {
            var claimsIdentity = user.Identity as ClaimsIdentity;
            if (claimsIdentity != null && (int.TryParse(claimsIdentity.FindFirst(ClaimTypes.Name)?.Value, out int userId)))
            {
                return userId;
            }

            return null;
        }

        public bool ValidateUser(UserLoginModel userLoginModel)
        {
            if(GetUsers().Any(user => user.Email == userLoginModel.Email))
            {
                return false;
            }
            return !string.IsNullOrEmpty(userLoginModel.Password) && new EmailAddressAttribute().IsValid(userLoginModel.Email);
        }

        private ClaimsIdentity GetIdentity(UserEntity userEntity)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, userEntity.Id.ToString()),
                new Claim(ClaimTypes.Role, userEntity.Role.GetString())
            };
            ClaimsIdentity claimsIdentity =
                new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                    ClaimsIdentity.DefaultRoleClaimType);
            return claimsIdentity;
        }

        private string GetJWT(ClaimsIdentity identity)
        {
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                issuer: AuthOptions.ISSUER,
                audience: AuthOptions.AUDIENCE,
                notBefore: now,
                claims: identity.Claims,
                expires: now.Add(TimeSpan.FromDays(AuthOptions.LIFETIME)),
                signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            return encodedJwt;
        }
    }
}