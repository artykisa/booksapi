using System;
using System.Collections.Generic;
using System.Linq;
using BooksApi.DataAccess.Entities;
using BooksApi.DataAccess.Repositories.Interfaces;
using BooksApi.Services.Services.Interfaces;

namespace BooksApi.Services.Services
{
    public class BookService : IBookService
    {
        private readonly IBookRepository _bookRepository;

        public BookService(IBookRepository bookRepository)
        {
            _bookRepository = bookRepository;
        }
        
        public BookEntity RegisterBook(BookEntity bookEntity)
        {
            var book = _bookRepository.RegisterBook(bookEntity);
            return book;
        }

        public BookEntity GetBook(int id)
        {
            var book = _bookRepository.GetBook(id);
            return book;
        }

        public List<BookEntity> GetBooks()
        {
            var books = _bookRepository.GetBooks();
            return books;
        }

        public List<BookEntity> GetExpiredBooks()
        {
            var books = _bookRepository.GetBooks().Where(book => book.DeadLine < DateTime.Now).ToList();
            return books;
        }

        public List<BookEntity> GetAvailableBooks()
        {
            var books = _bookRepository.GetBooks().Where(book => book.DeadLine >= DateTime.Now).ToList();
            return books;
        }

        public List<BookEntity> GetBooksFromCart(int userId)
        {
            var books = _bookRepository.GetBooks()
                .Where(book => book.UserCarts.Select(user => user.Id).Contains(userId)).ToList();
            return books;
        }

        public BookEntity UpdateBook(BookEntity bookEntity)
        {
            var returnBookEntity = _bookRepository.UpdateBook(bookEntity);
            return returnBookEntity;
        }

        public void DeleteBook(int id)
        {
            _bookRepository.DeleteBook(id);
        }
    }
}