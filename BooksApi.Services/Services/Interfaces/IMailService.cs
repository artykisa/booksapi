using System.Threading.Tasks;
using BooksApi.DataAccess.Entities;

namespace BooksApi.Services.Services.Interfaces
{
    public interface IMailService
    {
        Task SendEmailAsync(string email, string subject, string message);
        void SendOrderEmail(OrderEntity orderEntity);
    }
}