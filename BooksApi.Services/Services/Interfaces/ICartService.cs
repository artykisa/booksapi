using BooksApi.DataAccess.Entities;
using BooksApi.Services.Models;

namespace BooksApi.Services.Services.Interfaces
{
    public interface ICartService
    {
        CartModel GetCart(int userId);
        CartModel CleanCart(int userId);
        CartModel AddBook(int userId, int bookId);
        CartModel RemoveOneBook(int userId, int bookId);
        CartModel RemoveBook(int userId, int bookId);
        CartModel OrderCart(int userId, CartOrderModel cartOrderModel);
    }
}