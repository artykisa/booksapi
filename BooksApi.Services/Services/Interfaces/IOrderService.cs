using System.Collections.Generic;
using BooksApi.DataAccess.Entities;
using BooksApi.Services.Models;

namespace BooksApi.Services.Services.Interfaces
{
    public interface IOrderService
    {
        List<OrderModel> GetAllOrders();
        OrderModel GetOrder(int id);
        OrderEntity UpdateOrderEntity(OrderEntity orderEntity);
    }
}