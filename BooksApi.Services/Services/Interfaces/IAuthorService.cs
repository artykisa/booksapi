using System.Collections.Generic;
using BooksApi.DataAccess.Entities;

namespace BooksApi.Services.Services.Interfaces
{
    public interface IAuthorService
    {
        AuthorEntity RegisterAuthor(AuthorEntity authorEntity);
        AuthorEntity GetAuthor(int id);
        List<AuthorEntity> GetAuthors();
        AuthorEntity UpdateAuthor(AuthorEntity authorEntity);
        void DeleteAuthor(int id);
    }
}