using System.Collections.Generic;
using BooksApi.DataAccess.Entities;

namespace BooksApi.Services.Services.Interfaces
{
    public interface IBookService
    {
        BookEntity RegisterBook(BookEntity bookEntity);
        BookEntity GetBook(int id);
        List<BookEntity> GetBooks();
        List<BookEntity> GetExpiredBooks();
        List<BookEntity> GetAvailableBooks();
        List<BookEntity> GetBooksFromCart(int userId);
        BookEntity UpdateBook(BookEntity bookEntity);
        void DeleteBook(int id);
    }
}