using System.Collections.Generic;
using System.Security.Claims;
using BooksApi.DataAccess.Entities;
using BooksApi.Services.Models;
using BooksApi.Web.ViewModels;

namespace BooksApi.Services.Services.Interfaces
{
    public interface IUserService
    {
        UserEntity RegisterUser(UserEntity userEntity);
        UserEntity GetUser(int id);
        List<UserEntity> GetUsers();
        UserAccessModel LoginUser(UserEntity userEntity);
        int? GetUserIdFromJWT(ClaimsPrincipal user);
        bool ValidateUser(UserLoginModel userLoginModel);
        UserEntity UpdateUser(UserEntity userEntity);
        UserEntity GiveAdminPermissions(int userId);
        UserEntity RemoveAdminPermissions(int userId);
    }
}