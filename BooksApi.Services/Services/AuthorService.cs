using System.Collections.Generic;
using BooksApi.DataAccess.Entities;
using BooksApi.DataAccess.Repositories.Interfaces;
using BooksApi.Services.Services.Interfaces;

namespace BooksApi.Services.Services
{
    public class AuthorService : IAuthorService
    {
        private readonly IAuthorRepository _authorRepository;

        public AuthorService(IAuthorRepository authorRepository)
        {
            _authorRepository = authorRepository;
        }
        
        public AuthorEntity RegisterAuthor(AuthorEntity authorEntity)
        {
            var author = _authorRepository.RegisterAuthor(authorEntity);
            return author;
        }

        public AuthorEntity GetAuthor(int id)
        {
            var author = _authorRepository.GetAuthor(id);
            return author;
        }

        public List<AuthorEntity> GetAuthors()
        {
            var authors = _authorRepository.GetAuthors();
            return authors;
        }

        public AuthorEntity UpdateAuthor(AuthorEntity authorEntity)
        {
            var author = _authorRepository.UpdateAuthor(authorEntity);
            return author;
        }

        public void DeleteAuthor(int id)
        {
            _authorRepository.DeleteAuthor(id);
        }
    }
}