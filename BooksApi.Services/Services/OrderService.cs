using System.Collections.Generic;
using AutoMapper;
using BooksApi.DataAccess.Entities;
using BooksApi.DataAccess.Repositories.Interfaces;
using BooksApi.Services.Models;
using BooksApi.Services.Services.Interfaces;

namespace BooksApi.Services.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IMapper _mapper;

        public OrderService(IOrderRepository orderRepository, IMapper mapper)
        {
            _orderRepository = orderRepository;
            _mapper = mapper;
        }
        public List<OrderModel> GetAllOrders()
        {
            var orders = _mapper.Map<List<OrderModel>>(_orderRepository.GetAllOrders());
            foreach (var order in orders)
            {
                foreach (var book in order.Books)
                {
                    book.Count = _orderRepository.GetCount(order.Id, book.Id);
                }
            }
            return orders;
        }

        public OrderModel GetOrder(int id)
        {
            var order = _mapper.Map<OrderModel>(_orderRepository.GetOrder(id));
            foreach (var book in order.Books)
            {
                book.Count = _orderRepository.GetCount(order.Id, book.Id);
            }
            return order;
        }

        public OrderEntity UpdateOrderEntity(OrderEntity orderEntity)
        {
            var order = _orderRepository.GetOrder(orderEntity.Id);
            order.Status = orderEntity.Status;
            var returnOrder = _orderRepository.UpdateOrderEntity(order);
            return returnOrder;
        }
    }
}