using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace BooksApi.Services.Helpers
{
    public static class AuthOptions
    {
        public const string ISSUER = "BooksApiServer"; 
        public const string AUDIENCE = "BooksApiClient"; 
        const string KEY = "KEYKEYKEYREALLYSECRET123321";   
        public const int LIFETIME = 10; 
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}