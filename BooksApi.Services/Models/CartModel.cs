using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using BooksApi.DataAccess.Entities;

namespace BooksApi.Services.Models
{
    public class CartModel
    {
        public int UserId { get; set; }
        public IList<CartBookModel> Books { get; set; }
    }
}