namespace BooksApi.Services.Models
{
    public class UserAccessModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string AccessToken { get; set; }
    }
}