using System;
using System.Collections.Generic;
using BooksApi.DataAccess.Entities;
using BooksApi.DataAccess.Enums;

namespace BooksApi.Services.Models
{
    public class CartBookModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt  {get; set; }
        public DateTime UpdatedAt { get; set; }
        public BookStatus Status { get; set; }
        public DateTime DeadLine { get; set; }
        public byte[] Image { get; set; }
        public AuthorEntity Author { get; set; }
        public int? Count { get; set; }
    }
}