using BooksApi.DataAccess.Entities;

namespace BooksApi.Services.Models
{
    public class CartOrderModel
    {
        public int Bonuses { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public AddressEntity Address { get; set; }
    }
}