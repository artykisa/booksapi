using System.Collections.Generic;
using BooksApi.DataAccess.Entities;
using BooksApi.DataAccess.Enums;

namespace BooksApi.Services.Models
{
    public class OrderModel
    {
        public int Id { get; set; }
        public double Cost { get; set; }
        public double FinalCost { get; set; }
        public int Bonuses { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public OrderStatus Status { get; set; }
        public UserEntity User { get; set; }
        public List<CartBookModel> Books { get; set; }
        public AddressEntity Address { get; set; }
    }
}