using System;
using System.Data.Common;

namespace BooksApi.DataAccess.Utility
{
    public static class CartItemCountHelper
    {
        public const string UPDATE_VALUE = "UpdateCartItemCount";
        public const string GET_VALUE = "GetCartItemCount";

        public static int? Get(this DbDataReader reader)
        {
            int? count = null;
            while (reader.Read())
            {
                count = ConvertFromDBVal<int?>(reader["Count"]);
            }

            return count;

        }
        
        private static T ConvertFromDBVal<T>(object obj)
        {
            if (obj == null || obj.ToString() == null || obj.ToString() == String.Empty)
            {
                return default(T); // returns the default value for the type
            }
            else
            {
                return (T)obj;
            }
        }
    }
}