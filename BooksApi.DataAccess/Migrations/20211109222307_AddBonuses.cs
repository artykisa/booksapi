﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BooksApi.DataAccess.Migrations
{
    public partial class AddBonuses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Bonuses",
                table: "Users",
                type: "int",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Bonuses",
                table: "Users");
        }
    }
}
