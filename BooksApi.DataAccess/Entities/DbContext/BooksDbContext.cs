
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace BooksApi.DataAccess.Entities.DbContext
{
    public class BooksDbContext : Microsoft.EntityFrameworkCore.DbContext, IBooksDbContext
    {
        public static string ConnectionString = @"Server=localhost;Database=BooksApi;Integrated Security=True";
        public BooksDbContext()
        {
        }

        public BooksDbContext(DbContextOptions<BooksDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<BookEntity> Books { get; set; }

        public virtual DbSet<UserEntity> Users { get; set; }
        public DbSet<AuthorEntity> Authors { get; set; }
        public DbSet<OrderEntity> Orders { get; set; }
        public DbSet<AddressEntity> Address { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConnectionString);
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Cyrillic_General_CI_AS");

            modelBuilder.Entity<BookEntity>(entity =>
            {
                entity.HasMany(p => p.UserCarts)
                    .WithMany(t => t.BooksCart)
                    .UsingEntity<Dictionary<string, object>>(
                        "BookUser",
                        j => j
                            .HasOne<UserEntity>()
                            .WithMany()
                            .HasForeignKey("UserId")
                            .HasConstraintName("FK_BookUser_User"),
                        j => j

                            .HasOne<BookEntity>()
                            .WithMany()
                            .HasForeignKey("BookId")
                            .HasConstraintName("FK_BookUser_Book")
                    );
                
                entity.HasMany(p => p.Orders)
                    .WithMany(t => t.Books)
                    .UsingEntity<Dictionary<string, object>>(
                        "BookOrder",
                        j => j
                            .HasOne<OrderEntity>()
                            .WithMany()
                            .HasForeignKey("OrderId")
                            .HasConstraintName("FK_BookOrder_Order"),
                        j => j

                            .HasOne<BookEntity>()
                            .WithMany()
                            .HasForeignKey("BookId")
                            .HasConstraintName("FK_BookOrder_Book")
                    );

                entity.ToTable("Books");
            });
            modelBuilder.Entity<UserEntity>(entity =>
                {
                    entity.HasMany(p => p.BooksCart)
                        .WithMany(t => t.UserCarts)
                        .UsingEntity<Dictionary<string, object>>(
                            "BookUser",
                            j => j
                                .HasOne<BookEntity>()
                                .WithMany()
                                .HasForeignKey("BookId")
                                .HasConstraintName("FK_BookUser_Book"),
                            j => j

                                .HasOne<UserEntity>()
                                .WithMany()
                                .HasForeignKey("UserId")
                                .HasConstraintName("FK_BookUser_User")
                        );
                    entity.ToTable("Users");
                }
            );
            modelBuilder.Entity<AuthorEntity>(entity =>
            {
                entity.HasMany(e => e.Books)
                    .WithOne(e => e.Author);
                entity.ToTable("Authors");
            });
            
            modelBuilder.Entity<OrderEntity>(entity =>
            {
                entity.HasMany(e => e.Books)
                    .WithMany(t => t.Orders)
                    .UsingEntity<Dictionary<string, object>>(
                        "BookOrder",
                        j => j
                            .HasOne<BookEntity>()
                            .WithMany()
                            .HasForeignKey("BookId")
                            .HasConstraintName("FK_BookOrder_Book"),
                        j => j
                            .HasOne<OrderEntity>()
                            .WithMany()
                            .HasForeignKey("OrderId")
                            .HasConstraintName("FK_BookOrder_Order")
                    );;
                entity.HasOne(e => e.User).WithMany(user => user.Orders);
                entity.HasOne(e => e.Address);
                entity.ToTable("Orders");
            });

        }

    }
}