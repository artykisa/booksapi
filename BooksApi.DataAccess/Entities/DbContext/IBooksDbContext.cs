using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace BooksApi.DataAccess.Entities.DbContext
{
    public interface IBooksDbContext
    {
        public DbSet<BookEntity> Books { get; set; }

        public DbSet<UserEntity> Users { get; set; }
        public DbSet<AuthorEntity> Authors { get; set; }
        public DbSet<OrderEntity> Orders { get; set; }
        public DbSet<AddressEntity> Address { get; set; }
        

        int SaveChanges();

        public DbSet<TEntity> Set<TEntity>()
                where TEntity : class;

        public EntityEntry<TEntity> Entry<TEntity>(TEntity entity)
                where TEntity : class;

    }
}