using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using BooksApi.DataAccess.Enums;

namespace BooksApi.DataAccess.Entities
{
    [Table("Books")]
    public class BookEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt  {get; set; }
        public DateTime UpdatedAt { get; set; }
        public BookStatus Status { get; set; }
        public DateTime DeadLine { get; set; }
        public AuthorEntity Author { get; set; }
        public IList<UserEntity> UserCarts { get; set; }
        public IList<OrderEntity> Orders { get; set; }
        public byte[] Image { get; set; }
    }
}