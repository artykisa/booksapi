using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using BooksApi.DataAccess.Enums;

namespace BooksApi.DataAccess.Entities
{
    [Table("Users")]
    public class UserEntity
    {
        public int Id { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public double? Bonuses { get; set; }
        public UserRole Role { get; set; }
        public IList<BookEntity> BooksCart { get; set; }
        public IList<OrderEntity> Orders { get; set; }
    }
}