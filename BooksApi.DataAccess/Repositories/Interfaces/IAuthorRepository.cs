using System.Collections.Generic;
using BooksApi.DataAccess.Entities;

namespace BooksApi.DataAccess.Repositories.Interfaces
{
    public interface IAuthorRepository
    {
        AuthorEntity RegisterAuthor(AuthorEntity authorEntity);
        AuthorEntity GetAuthor(int id);
        List<AuthorEntity> GetAuthors();
        AuthorEntity UpdateAuthor(AuthorEntity authorEntity);
        void DeleteAuthor(int id);
    }
}