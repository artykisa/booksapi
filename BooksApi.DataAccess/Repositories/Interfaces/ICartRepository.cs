namespace BooksApi.DataAccess.Repositories.Interfaces
{
    public interface ICartRepository
    {
        void Update(int userId, int bookId, int count);
        int? Get(int userId, int bookId);
    }
}