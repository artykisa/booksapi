using System.Collections.Generic;
using BooksApi.DataAccess.Entities;

namespace BooksApi.DataAccess.Repositories.Interfaces
{
    public interface IOrderRepository
    {
        List<OrderEntity> GetAllOrders();
        OrderEntity GetOrder(int id);
        OrderEntity UpdateOrderEntity(OrderEntity orderEntity);
        public int? GetCount(int userId, int bookId);
        public void UpdateCount(int userId, int bookId, int count);
    }
}