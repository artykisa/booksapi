using System.Collections.Generic;
using BooksApi.DataAccess.Entities;

namespace BooksApi.DataAccess.Repositories.Interfaces
{
    public interface IUserRepository
    {
        UserEntity RegisterUser(UserEntity userEntity);
        UserEntity GetUser(int id);
        List<UserEntity> GetUsers();
        UserEntity LoginUser(UserEntity userEntity);
        UserEntity UpdateUser(UserEntity userEntity);
    }
}