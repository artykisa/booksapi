using System.Collections.Generic;
using BooksApi.DataAccess.Entities;

namespace BooksApi.DataAccess.Repositories.Interfaces
{
    public interface IBookRepository
    {
        BookEntity RegisterBook(BookEntity bookEntity);
        BookEntity GetBook(int id);
        List<BookEntity> GetBooks();
        BookEntity UpdateBook(BookEntity bookEntity);
        void DeleteBook(int id);
    }
}