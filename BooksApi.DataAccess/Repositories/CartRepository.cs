using System.Collections.Generic;
using System.Data.Common;
using BooksApi.DataAccess.Entities.DbContext;
using BooksApi.DataAccess.Repositories.Interfaces;
using BooksApi.DataAccess.Utility;
using Microsoft.VisualBasic;

namespace BooksApi.DataAccess.Repositories
{
    public class CartRepository : BaseRepository, ICartRepository
    {
        public int? Get(int userId, int bookId)
        {
            var count = ExecuteCommand<int?>(CartItemCountHelper.GET_VALUE,
                CartItemCountHelper.Get,
                GetParameters(userId, bookId).ToArray());
            return count;

        }

        public void Update(int userId, int bookId, int count)
        {
            ExecuteNonQuery(CartItemCountHelper.UPDATE_VALUE,GetParameters(userId,bookId,count).ToArray());
        }
    }
}