using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection.Metadata;
using BooksApi.DataAccess.Entities;
using BooksApi.DataAccess.Entities.DbContext;
using BooksApi.DataAccess.Repositories.Interfaces;
using BooksApi.DataAccess.Utility;
using Microsoft.EntityFrameworkCore;

namespace BooksApi.DataAccess.Repositories
{
    public class OrderRepository: BaseRepository, IOrderRepository
    {
        private readonly IBooksDbContext _dbContext;

        public OrderRepository(IBooksDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public List<OrderEntity> GetAllOrders()
        {
            var orders = _dbContext.Orders.Include(order => order.Books).Include(order => order.User).Include(order => order.Address).ToList();
            return orders;
        }

        public OrderEntity GetOrder(int id)
        {
            var orderEntity = _dbContext.Orders.Include(order => order.Books).Include(order => order.User).Include(order => order.Address).FirstOrDefault(order => order.Id == id);
            return orderEntity;
        }

        public OrderEntity UpdateOrderEntity(OrderEntity orderEntity)
        {
            var returnOrderEntity = _dbContext.Orders.Update(orderEntity);
            _dbContext.SaveChanges();
            return returnOrderEntity.Entity;
        }
        
        public int? GetCount(int orderId, int bookId)
        {
            var count = ExecuteCommand<int?>(OrderItemCountHelper.GET_VALUE,
                OrderItemCountHelper.Get,
                OrderParameters(orderId, bookId).ToArray());
            return count;

        }

        public void UpdateCount(int orderId, int bookId, int count)
        {
            ExecuteNonQuery(OrderItemCountHelper.UPDATE_VALUE,OrderParameters(orderId,bookId,count).ToArray());
        }

        private List<DbParameter> OrderParameters(int orderId, int bookId,int? count = null)
        {
            var orderIdParameter = CreateParameter("@order_id", DbType.Int32, orderId);
            var bookIdParameter = CreateParameter("@book_id", DbType.Int32, bookId);
            var parameterList = new List<DbParameter>();
            if (count != null)
            {
                var countParameter = CreateParameter("@count", DbType.Int32, count.Value);
                parameterList.Add(countParameter);
            }
            parameterList.Add(orderIdParameter);
            parameterList.Add(bookIdParameter);
            return parameterList;
        }
    }
}