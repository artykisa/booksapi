using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using BooksApi.DataAccess.Entities;
using BooksApi.DataAccess.Entities.DbContext;
using BooksApi.DataAccess.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BooksApi.DataAccess.Repositories
{
    public class UserRepository : BaseRepository, IUserRepository
    {
        private readonly IBooksDbContext _dbContext;
        public UserRepository(IBooksDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public UserEntity RegisterUser(UserEntity userEntity)
        {
            _dbContext.Users.Add(userEntity);
            _dbContext.SaveChanges();
            return GetUser(userEntity.Id);
        }

        public UserEntity GetUser(int id)
        {
            var user = _dbContext.Users.Include(userEntity => userEntity.BooksCart).FirstOrDefault(entity => entity.Id == id);
            return user;
        }

        public List<UserEntity> GetUsers()
        {
            var users = _dbContext.Users.Include(userEntity => userEntity.BooksCart).ToList();
            return users;
        }

        public UserEntity LoginUser(UserEntity userEntity)
        {
            var user = _dbContext.Users.AsNoTracking().FirstOrDefault(entity => entity.Email == userEntity.Email && entity.Password == userEntity.Password);
            return user;
        }

        public UserEntity UpdateUser(UserEntity userEntity)
        {
            var returnUserEntity = _dbContext.Users.Update(userEntity);
            _dbContext.SaveChanges();
            return returnUserEntity.Entity;
        }

        private string GetHash(string password)
        {
            return password.GetHashCode().ToString();
        }
    }
}