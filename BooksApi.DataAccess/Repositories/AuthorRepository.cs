using System.Collections.Generic;
using System.Linq;
using BooksApi.DataAccess.Entities;
using BooksApi.DataAccess.Entities.DbContext;
using BooksApi.DataAccess.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BooksApi.DataAccess.Repositories
{
    public class AuthorRepository : IAuthorRepository
    {
        private readonly IBooksDbContext _dbContext;

        public AuthorRepository(IBooksDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public AuthorEntity RegisterAuthor(AuthorEntity authorEntity)
        {
            var returnAuthorEntity = _dbContext.Authors.Update(authorEntity);
            _dbContext.SaveChanges();
            return GetAuthor(returnAuthorEntity.Entity.Id);
        }

        public AuthorEntity GetAuthor(int id)
        {
            var authorEntity = _dbContext.Authors.Include(author => author.Books).FirstOrDefault(author => author.Id == id);
            return authorEntity;
        }

        public List<AuthorEntity> GetAuthors()
        {
            var authorEntities = _dbContext.Authors.Include(author => author.Books).ToList();
            return authorEntities;
        }

        public AuthorEntity UpdateAuthor(AuthorEntity authorEntity)
        {
            var returnAuthorEntity = _dbContext.Authors.Update(authorEntity);
            _dbContext.SaveChanges();
            return returnAuthorEntity.Entity;
        }

        public void DeleteAuthor(int id)
        {
            var author = GetAuthor(id);
            _dbContext.Authors.Remove(author);
            _dbContext.SaveChanges();
        }
    }
}