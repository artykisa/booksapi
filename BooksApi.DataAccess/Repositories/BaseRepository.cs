using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using BooksApi.DataAccess.Entities.DbContext;
using Microsoft.Extensions.Logging;

namespace BooksApi.DataAccess.Repositories
{
    public class BaseRepository
    {
        protected readonly DbProviderFactory Provider;
        private readonly string _connectionString;
        
        public BaseRepository()
        {
            Provider = DbProviderFactories.GetFactory("System.Data.SqlClient");
            _connectionString = BooksDbContext.ConnectionString;
        }
        
        private DbConnection GetConnection()
        {
            var dbConnection = Provider.CreateConnection();
            dbConnection.ConnectionString = _connectionString;
            return dbConnection;
        }

        
        protected T ExecuteCommand<T>(string procedureName, Func<DbDataReader, T> readerFunction,
            params DbParameter[] dbParameters)
        {
            using var dbCommand = Provider.CreateCommand();

            SetupCommand(dbCommand, procedureName, dbParameters);

            T item;
            try
            {
                dbCommand.Connection.Open();
                using DbDataReader reader = dbCommand.ExecuteReader();
                item = readerFunction(reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new DataException($"{dbCommand.CommandText}\n{ex.Message}");
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Connection.Dispose();
            }

            return item;
        }

        private void SetupCommand(DbCommand dbCommand, string procedureName, params DbParameter[] dbParameters)
        {
            if (dbCommand == null)
            {
                throw new ArgumentNullException("DbProvider Exception");
            }

            dbCommand.CommandText = procedureName;
            dbCommand.CommandType = CommandType.StoredProcedure;
            dbCommand.Connection = GetConnection();

            if (dbCommand.Connection == null)
            {
                throw new ArgumentNullException("DbConnection Exception");
            }

            foreach (var parameter in dbParameters)
            {
                dbCommand.Parameters.Add(parameter);
            }
        }

        protected void ExecuteNonQuery(string procedureName, params DbParameter[] dbParameters)
        {
            using var dbCommand = Provider.CreateCommand();

            SetupCommand(dbCommand, procedureName, dbParameters);

            try
            {
                dbCommand.Connection.Open();
                dbCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new DataException($"Query Exception\n{dbCommand.CommandText}\n{ex.Message}\n");
            }
            finally
            {
                dbCommand.Connection.Close();
                dbCommand.Connection.Dispose();
            }
        }
        
        protected DbParameter CreateParameter(string parameterName, DbType dbType, object value)
        {
            var dbParameter = Provider.CreateParameter();
            dbParameter.ParameterName = parameterName;
            dbParameter.DbType = dbType;
            dbParameter.Value = value;
            return dbParameter;
        }
        
        protected List<DbParameter> GetParameters(int userId, int bookId, int? count = null)
        {
            var userIdParameter = CreateParameter("@user_id", DbType.Int32, userId);
            var bookIdParameter = CreateParameter("@book_id", DbType.Int32, bookId);
            var parameterList = new List<DbParameter>();
            if (count != null)
            {
                var countParameter = CreateParameter("@count", DbType.Int32, count.Value);
                parameterList.Add(countParameter);
            }
            parameterList.Add(userIdParameter);
            parameterList.Add(bookIdParameter);
            return parameterList;
        }
    }
}