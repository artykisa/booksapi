using System.Collections.Generic;
using System.Linq;
using BooksApi.DataAccess.Entities;
using BooksApi.DataAccess.Entities.DbContext;
using BooksApi.DataAccess.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BooksApi.DataAccess.Repositories
{
    public class BookRepository : BaseRepository, IBookRepository
    {
        private readonly IBooksDbContext _dbContext;

        public BookRepository(IBooksDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public BookEntity RegisterBook(BookEntity bookEntity)
        {
            bookEntity.UserCarts = new List<UserEntity>();
            bookEntity.Orders = new List<OrderEntity>();
            var returnBookEntity = _dbContext.Books.Update(bookEntity);
            _dbContext.SaveChanges();
            return GetBook(returnBookEntity.Entity.Id);
        }

        public BookEntity GetBook(int id)
        {
            var book = _dbContext.Books.Include(book => book.UserCarts).Include(book=> book.Author).FirstOrDefault(entity => entity.Id == id);
            if (book != null)
            {
                book.UserCarts ??= new List<UserEntity>();
                book.Orders ??= new List<OrderEntity>();
            }
            return book;
        }

        public List<BookEntity> GetBooks()
        {
            var books = _dbContext.Books.Include(book => book.UserCarts).Include(book=> book.Author).ToList().Select(book =>
            {
                book.UserCarts ??= new List<UserEntity>();
                return book;
            }).ToList();;
            
            return books;
        }

        public BookEntity UpdateBook(BookEntity bookEntity)
        {
            var returnBookEntity = _dbContext.Books.Update(bookEntity);
            _dbContext.SaveChanges();
            return returnBookEntity.Entity;
        }

        public void DeleteBook(int id)
        {
            var book = GetBook(id);
            _dbContext.Books.Remove(book);
            _dbContext.SaveChanges();
        }
    }
}