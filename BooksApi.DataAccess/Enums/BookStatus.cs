namespace BooksApi.DataAccess.Enums
{
    public enum BookStatus
    {
        Purchased = 1, 
        NotPurchased, 
        InCart
    }
}