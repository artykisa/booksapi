using BooksApi.DataAccess.Entities;

namespace BooksApi.DataAccess.Enums
{
    public enum UserRole
    {
        Default = 0,
        Admin = 1
    }
    
    public static class UserRoleExtensions
    {
        public const string DefaultRole = "Default";
        public const string AdminRole = "Admin";
        public static string GetString(this UserRole userRole)
        {
            return userRole switch
            {
                UserRole.Default => DefaultRole,
                UserRole.Admin => AdminRole,
                _ => "No role"
            };
        }
    }
}