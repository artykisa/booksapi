namespace BooksApi.DataAccess.Enums
{
    public enum OrderStatus
    {
        Ordered = 1,
        Delivered = 2
    }
}